// { "framework": "Vue" }
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__
	var __vue_styles__ = []

	/* styles */
	__vue_styles__.push(__webpack_require__(1)
	)

	/* script */
	__vue_exports__ = __webpack_require__(2)

	/* template */
	var __vue_template__ = __webpack_require__(3)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}
	__vue_options__.__file = "C:\\weex-refinn\\src\\App.vue"
	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-21b17af8"
	__vue_options__.style = __vue_options__.style || {}
	__vue_styles__.forEach(function (module) {
	  for (var name in module) {
	    __vue_options__.style[name] = module[name]
	  }
	})
	if (typeof __register_static_styles__ === "function") {
	  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
	}

	module.exports = __vue_exports__
	module.exports.el = 'true'
	new Vue(module.exports)


/***/ }),
/* 1 */
/***/ (function(module, exports) {

	module.exports = {
	  "wrapper": {
	    "flexDirection": "column",
	    "justifyContent": "center",
	    "display": "block",
	    "padding": 30,
	    "width": 100
	  },
	  "panel": {
	    "backgroundColor": "#ecf0f1",
	    "boxShadow": "10px 10px 5px grey",
	    "margin": 15,
	    "marginBottom": 50
	  },
	  "panel-title": {
	    "fontSize": 35,
	    "color": "#FFFFFF",
	    "letterSpacing": 3,
	    "padding": 20
	  },
	  "panel-header": {
	    "backgroundColor": "#3498db",
	    "color": "#ecf0f1"
	  },
	  "group-wrapper": {
	    "padding": 30
	  },
	  "group": {
	    "flexDirection": "row",
	    "justifyContent": "center",
	    "marginBottom": 10,
	    "display": "flex"
	  },
	  "title": {
	    "fontSize": 35,
	    "color": "#888888",
	    "letterSpacing": 2
	  },
	  "data": {
	    "fontSize": 35,
	    "fontWeight": "bold",
	    "marginLeft": 12,
	    "color": "#41B883",
	    "letterSpacing": 1,
	    "flex": 1
	  },
	  "button": {
	    "backgroundColor": "#4CAF50",
	    "border": "none",
	    "color": "#FFFFFF",
	    "textAlign": "center",
	    "textDecoration": "none",
	    "display": "inline-block",
	    "fontSize": 16
	  }
	}

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//

	var modal = weex.requireModule('modal');
	var stream = weex.requireModule('stream');
	exports.default = {
	  data: function data() {
	    return {
	      reload_msg: 'Fetching data..',
	      pageview: this.reload_msg,
	      submitted: this.reload_msg,
	      approved: this.reload_msg

	    };
	  },

	  methods: {
	    onlongpress: function onlongpress(event) {
	      console.log('onlongpress:', event);
	      this.reload();
	      modal.toast({

	        message: this.reload_msg,
	        duration: 0.8
	      });
	    },
	    refinnAPI: function refinnAPI(callback) {
	      return stream.fetch({
	        method: 'GET',
	        type: 'json',
	        url: 'http://weex-api.dev/api/getarray'
	      }, callback);
	    },

	    reload: function reload() {
	      var _this = this;

	      this.pageview = this.reload_msg;
	      this.submitted = this.reload_msg;
	      this.approved = this.reload_msg;

	      this.refinnAPI(function (res) {
	        _this.pageview = res.ok ? res.data.pageview : '(network error)';
	        _this.submitted = res.ok ? res.data.summited : '(network error)';
	        _this.approved = res.ok ? res.data.approved : '(network error)';
	      });
	    }
	  },
	  created: function created() {
	    this.reload();
	  }
	};
	module.exports = exports['default'];

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: ["wrapper"],
	    on: {
	      "longpress": _vm.onlongpress
	    }
	  }, [_c('div', {
	    staticClass: ["panel"]
	  }, [_vm._m(0), _c('div', {
	    staticClass: ["panel-content"]
	  }, [_c('div', {
	    staticClass: ["group-wrapper"]
	  }, [_c('div', {
	    staticClass: ["group"]
	  }, [_c('text', {
	    staticClass: ["title"]
	  }, [_vm._v("Unique Visitor :")]), _c('text', {
	    staticClass: ["data"]
	  }, [_vm._v(_vm._s(_vm.pageview))])]), _c('div', {
	    staticClass: ["group"]
	  }, [_c('text', {
	    staticClass: ["title"]
	  }, [_vm._v("Submitted :")]), _c('text', {
	    staticClass: ["data"]
	  }, [_vm._v(_vm._s(_vm.submitted))])]), _c('div', {
	    staticClass: ["group"]
	  }, [_c('text', {
	    staticClass: ["title"]
	  }, [_vm._v("Approved :")]), _c('text', {
	    staticClass: ["data"]
	  }, [_vm._v(_vm._s(_vm.approved))])])])])]), _vm._m(1)])
	},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: ["panel-header"]
	  }, [_c('text', {
	    staticClass: ["panel-title"]
	  }, [_vm._v("Refinn Realtime Report")])])
	},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
	  return _c('div', {
	    staticClass: ["panel"]
	  }, [_c('div', {
	    staticClass: ["panel-header"]
	  }, [_c('text', {
	    staticClass: ["panel-title"]
	  }, [_vm._v("User Online")])]), _c('div', {
	    staticClass: ["panel-content"]
	  }, [_c('div', {
	    staticClass: ["group-wrapper"]
	  }, [_c('div', {
	    staticClass: ["group"]
	  }, [_c('text', {
	    staticClass: ["title"]
	  }, [_vm._v("Registered User :")]), _c('text', {
	    staticClass: ["data"]
	  }, [_vm._v("10")])]), _c('div', {
	    staticClass: ["group"]
	  }, [_c('text', {
	    staticClass: ["title"]
	  }, [_vm._v("Guest :")]), _c('text', {
	    staticClass: ["data"]
	  }, [_vm._v("50")])])])])])
	}]}
	module.exports.render._withStripped = true

/***/ })
/******/ ]);